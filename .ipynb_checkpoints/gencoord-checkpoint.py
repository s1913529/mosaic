from mosaic_molecule import Compound
import pandas as pd


df = pd.read_csv("pointgroupsdata.csv")
compounds = df["Name"].tolist()
removed = []
for compound in compounds:
    molecule = Compound(compound)
    if molecule.cas_rn is None:
        removed.append(molecule.name)
        print(f"{molecule.name} is removed")
        
    else:
        molecule.gen3d()
        print(f"{molecule.name} generated")

with open("removedcompounds.txt", 'w') as f:
    for compound in removed:
        f.write(compound)

