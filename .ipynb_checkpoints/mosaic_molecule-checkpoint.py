"""
MOSAIC - Data Generation Module
Date: 12/06/2022

This programme supplies necessary methods
to obtain information when supplied a compound
name.

Patrick Wang
patrick.wang@physics.org
Version 0.1
"""

#import numpy as np
import pandas as pd
import requests
import json
#import openbabel as babel
import subprocess
import os
class APIError(Exception):
    pass

class Compound(object):
    """
    Class for a compound
    
        Properties:
        name: name of the compound, ideally IUPAC
        CAS-RN ID: CAS registry number for compound
        uri: CAS URI for REST API requests
        coord: atomic coordinates in xyz format

        Methods:
        __init__ Sends REST API request to cast
        __str__
        gen3d invokes openbabel in system shell to generate 3D coordinates

    """
    def __init__(self, name, smi = None, cas_rn = None, uri = None):
        """
        Initialises a molecule given its compound
        name.
        
        :param name: IUPAC/common name of the compound
        :param smi: SMILE string for the compound
        :param cas_rn: CAS registry number, if known
        :uri: CAS uri for rest API, if known
        """
        self.name = name
        self.smi = smi
        self.cas_rn = cas_rn
        #self.uri = uri
        if self.cas_rn is None:
            try:
                url = "https://commonchemistry.cas.org/api/search?q="
                r = requests.get(url + self.name)
                response_dict = r.json()
                self.cas_rn = response_dict['results'][0]['rn']
            except:
                pass

        #if self.uri is None:
            #self.uri = "substance/pt/" + "".join(e for e in self.cas_rn if e.isalnum())
        #else:
            #pass

        if self.smi is None:
            try:
                url = "https://commonchemistry.cas.org/api/detail?cas_rn="
                r = requests.get(url + self.cas_rn)
                self.smi = r.json()["smile"]
            except:
                pass
    def __str__(self):
        """
        Returns basic information on the compound

        """
        return f"{self.name}, CAS_RN:{self.cas_rn}."
    
    def gen3d(self, speed = 'best', path = "./coordinate_files"):
        """
        Method for generating .xyz file with name of compound. Invokes
        obabel command from system shell and generate xyz file for compound
        in a subdirectory ./coordinate_files (default)
        
        :param speed: choose from "best", "better", "fast", "fastest" for the
        accuracy of the minimization.
        """

        if os.path.exists(path):
            pass
        else:
            os.mkdir(path)
	    
        filename = f"{self.name}".replace(" ", "")
        
        with open(f"{filename}.smi", 'w') as f:
            f.write(self.smi)
        p = subprocess.Popen(f"obabel {filename}.smi -O {path}/{filename}.xyz --gen3d --{speed}", stdout=subprocess.PIPE, shell = True)

        with open(f"{filename}.smi", 'r') as f:
            coord = f.read()
        self.coord = coord
        return p, coord
        

