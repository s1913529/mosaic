"""
MOSAIC - Data Generation Module
Date: 12/06/2022

This programme supplies necessary methods
to obtain information when supplied a compound
name.

Patrick Wang
patrick.wang@physics.org
Version 0.6
"""

import numpy as np
from numpy.linalg import eigvals
from numpy.linalg import LinAlgError
import pandas as pd
import requests
import json
#import openbabel as babel
import subprocess
import os
class APIError(Exception):
    pass

class Compound(object):
    """
    Class for a compound
    
        Properties:
        name: name of the compound, ideally IUPAC
        CAS-RN ID: CAS registry number for compound
        uri: CAS URI for REST API requests
        coord: atomic coordinates in xyz format

        Methods:
        __init__ Sends REST API request to cast
        __str__
        gen3d invokes openbabel in system shell to generate 3D coordinates
        pos_matrix generates a list of atoms and their positions in numpy array of vectors
        is_planar uses principle component analysis to determine if a compound is planar

    """
    def __init__(self, name, smi = None, cas_rn = None, uri = None):
        """
        Initialises a molecule given its compound
        name.
        
        :param name: IUPAC/common name of the compound
        :param smi: SMILE string for the compound
        :param cas_rn: CAS registry number, if known
        :uri: CAS uri for rest API, if known
        """
        self.name = name
        self.smi = smi
        self.cas_rn = cas_rn
        #self.uri = uri
        if self.cas_rn is None:
            url = "https://commonchemistry.cas.org/api/search?q="
            r = requests.get(url + self.name)
            response_dict = r.json()
            status_code = r.status_code
            if status_code == 200:
                self.cas_rn = response_dict['results'][0]['rn']
            else:
                print(f'API Error, Code: {status_code}')
        else:
            pass

        #if self.uri is None:
            #self.uri = "substance/pt/" + "".join(e for e in self.cas_rn if e.isalnum())
        #else:
            #pass

        if self.smi is None:
            try:
                url = "https://commonchemistry.cas.org/api/detail?cas_rn="
                r = requests.get(url + self.cas_rn)
                self.smi = r.json()["smile"]
            except:
                pass
        self.atoms, self.pos = self.pos_matrix()
        self.cov = np.cov(self.pos - self.pos.mean(axis = 0), rowvar=False)
    def __str__(self):
        """
        Returns basic information on the compound

        """
        return f"{self.name}, CAS_RN:{self.cas_rn}."
    
    def gen3d(self, speed = 'best', path = "./coordinate_files"):
        """
        Method for generating .xyz file with name of compound. Invokes
        obabel command from system shell and generate xyz file for compound
        in a subdirectory ./coordinate_files (default)
        
        :param speed: choose from "best", "better", "fast", "fastest" for the
        accuracy of the minimization.
        :return p: Subprocess shell response
        :return coord: Coordinate in raw xyz-compliant string
        """

        if os.path.exists(path):
            pass
        else:
            os.mkdir(path)
        filename = f"{self.name}".replace(" ", "").lower()
        
        with open(f"{filename}.smi", 'w') as f:
            f.write(self.smi)
        p = subprocess.Popen(f"obabel {filename}.smi -O {path}/{filename}.xyz --gen3d --{speed}", stdout=subprocess.PIPE, shell = True)

        with open(f"{path}/{filename}.xyz", 'r') as f:
            coord = f.read()
        self.coord = coord
        return p, coord

    def pos_matrix(self, path="./coordinate_files"):
        """
        Method for generating an array with the coordinates and
        a list of atoms.
        :param path: Path to coordinate files
        :return atoms: List of atoms
        :return xyz_coordinates: numpy array of coordinates in 3D
        """
        try:
            atoms = []
            xyz_coordinates = []
            filename = f"{self.name}".replace(" ", "").lower()
            with open(f"{path}/{filename}.xyz") as f:
                for line_number, line in enumerate(f):
                    if line_number == 0:
                        num_atoms = int(line)
                    elif line_number == 1:
                        if "charge =" in line:
                            charge = int(line.split('=')[1])
                            
                        else:
                            charge = 0
                    else:
                        atomic_symbol, x, y, z = line.split()
                        atoms.append(atomic_symbol)
                        xyz_coordinates.append([float(x), float(y), float(z)])
        except:
            pass
        xyz_coordinates = np.array(xyz_coordinates)
        xyz_coordinates = xyz_coordinates - xyz_coordinates.mean(axis = 0)
        return atoms, xyz_coordinates
    
    def is_planar(self, tolerance = 1E-5):
        """
        Method to analyse if a molecule is planar within a
        tolerance.
        :param tolerance: Default to 1E-5, float
        :return bool: True if molecule is planar, false otherwise

        """
        var_matrix = self.cov
        try:
            if min(eigvals(var_matrix)) < tolerance:
                return True
            else:
                return False

        except LinAlgError:
            print("Eigenvalue computation did not converge.")
            return None
        
    def is_linear(self, tolerance = 1E-6):
        """
        Method to check if a molecule is linear within
        a tolerance.
        :param tolerance: Default 1E-6, float
        :return bool: True if molecule is linear, false otherwise
        """
        is_planar = self.is_planar()
        if is_planar is False:
            return False
        elif is_planar is True:
            atoms, coord = self.pos_matrix()
            var_matrix = np.cov(coord, rowvar=False)
            eig_vals = eigvals(var_matrix)
            eig_vals.sort()
            if eig_vals[0] < tolerance and eig_vals[1] < tolerance:
                return True
            else:
                return False

    def cov_plot(self):
        """
        Method to plot the eigenvectors of the
        covariance matrix using matplotlib 3D

        """
        atoms, pos = self.pos_matrix()
        pos = pos - pos.mean(axis = 0)
        X = np.tile(pos.mean(axis = 0)[0],3)
        Y = np.tile(pos.mean(axis = 0)[0],3)
        Z = np.tile(pos.mean(axis = 0)[0],3)
        cov = np.cov(pos, rowvar = False)
        eigvals, eigvects = eig(cov)
        U, V, W = eigvects[0,:], eigvects[1,:], eigvects[2,:]
        fig = plt.figure()
        ax = plt.axes(projection='3d')
        ax.scatter(pos[0::,0], pos[0::,1], pos[0::,2])
        ax.quiver(X,Y,Z,U,V,W, normalize = True, color=['red', 'blue', 'green'])
        ax.set_xlim([-3, 3])
        ax.set_ylim([-3, 3])
        ax.set_zlim([-3, 3])
        for atom, coord in zip(atoms,pos):
            ax.text(coord[0], coord[1], coord[2], f'{atom}')
        ax.set_box_aspect((1,1,1))

    
    @staticmethod
    def rot_gen(degeneracy, axis, mean):
        """
        Generates a rotation matrix about an arbitrary
        normalised vector using Rodrigues' Rotation Formula
        :param degneracy: int, specifies the degeneracy of rotation, n = 2 for 90 degree rotations
        :param axis: np array, a normalised vector of the axis of rotation
        :param mean: np array, a 1 by 3 array of mean position of molecules (origin)
        
        return: R, 3x3 np array that performs a rotation operation
        """
        axis = axis - mean
        x,y,z = axis
        theta = 360 / degeneracy
        theta = np.deg2rad(theta)
        R = np.array([[np.cos(theta) + x**2*(1-np.cos(theta)), x*y*(1-np.cos(theta)) - z*np.sin(theta), y*np.sin(theta) + x*z*(1-np.cos(theta))],
                    [z*np.sin(theta) + x*y*(1-np.cos(theta)), np.cos(theta) + y**2*(1-np.cos(theta)), -x*np.sin(theta) + y*z*(1-np.cos(theta))],
                    [-y*np.sin(theta) + x*z*(1-np.cos(theta)), x*np.sin(theta) + y*z*(1-np.cos(theta)), np.cos(theta) + z**2*(1-np.cos(theta))]
                    ])
        R[np.abs(R) < 1E-15] = 0
        return R
    
    @staticmethod
    def rotate(R, pos_matrix):
        new_pos = []
        for row, i in zip(pos_matrix, np.arange(len(pos_matrix))):
            new_pos.append(np.matmul(R, pos_matrix[i, 0::]))
        return np.array(new_pos)
    
    def r_value(self, R):
        old_pos = self.pos
        atoms = self.atoms
        new_pos = rotate(R, old_pos)
        